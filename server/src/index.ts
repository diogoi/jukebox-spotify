import express from "express";
import { Request, Response } from "express";
import * as bodyParser from "body-parser";

const app = express();

app.set("port", process.env.PORT || 5000);

app.use(bodyParser.json());

app.get("/", (req: Request, res: Response) => {
  res.send({ message: "Hello World" });
});

app.get("/test", (req: Request, res: Response) => {
  res.send({ message: "Hello World" });
});

app.get("/newtest", (req: Request, res: Response) => {
    res.send({ message: "Bye World" });
  });

app.listen(app.get("port"), function() {
  console.log("Server is running on port " + app.get("port"));
});
